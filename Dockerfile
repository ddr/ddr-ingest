FROM debian:bullseye

SHELL ["/bin/bash", "-c"]

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    TZ=US/Eastern

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install \
    bzip2 \
    curl \
    ffmpeg \
    file \
    imagemagick \
    less \
    locales \
    logrotate \
    mediainfo \
    pwgen \
    python3-jpylyzer \
    unzip \
    ; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/* ; \
    echo "$LANG UTF-8" >> /etc/locale.gen ; \
    locale-gen $LANG

# Download fester
RUN curl -sL -o /usr/local/bin/fester \
    https://gitlab.oit.duke.edu/ddr/fester/-/package_files/4722/download

#
# Env vars names FB_* are used by the filebrowser software.
# FILEBROWSER_* vars are local.
#

ENV FILEBROWSER_HOME="/opt/filebrowser"

WORKDIR $FILEBROWSER_HOME

# Bug affecting scope > v.2.21.1
# https://github.com/filebrowser/filebrowser/issues/2264
COPY --from=filebrowser/filebrowser:v2.21.1 /filebrowser ./

COPY . .

ENV FB_ADDRESS="0.0.0.0" \
    FB_AUTH_HEADER="X-FileBrowser-User" \
    FB_AUTH_METHOD="json" \
    FB_BASEURL="" \
    FB_BRANDING_DISABLEEXTERNAL="true" \
    FB_BRANDING_FILES="${FILEBROWSER_HOME}" \
    FB_BRANDING_NAME="File Browser" \
    FB_COMMANDS="cat,fester,ffprobe,file,find,grep,head,identify,jpylyzer,ls,md5sum,mediainfo,pwd,sha1sum,sha224sum,sha256sum,sha512sum,tail,zhead,zipinfo" \
    FB_DATABASE="${FILEBROWSER_HOME}/filebrowser.db" \
    FB_LOCALE="en" \
    FB_LOG="stdout" \
    FB_PASSWORD="changeme" \
    FB_PORT="8080" \
    FB_SCOPE="/nas" \
    FB_ROOT="/nas" \
    FB_USER="admin"

RUN set -eux; \
    mkdir -p $FB_ROOT ; \
    chgrp -R 0 . $FB_ROOT ; \
    chmod -R g=u . $FB_ROOT ; \
    chmod +x ./filebrowser; \
    useradd -r -u 1001 -g 0 filebrowser

VOLUME $FB_ROOT

HEALTHCHECK --start-period=2s --interval=5s --timeout=3s \
	CMD curl -f http://localhost:8080/health || exit 1

EXPOSE $FILEBROWSER_PORT

USER filebrowser

ENTRYPOINT ["/opt/filebrowser/entrypoint"]

CMD ["/opt/filebrowser/filebrowser"]
